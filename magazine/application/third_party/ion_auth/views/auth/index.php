<!DOCTYPE html>
<html>
<head>
	<title>Upload File</title>
</head>
<body>
	<h1>Upload Magazine</h1><hr>
	<a href="<?=base_url()?>admin/logout">Logout</a><hr>
	<?php echo $error;?>

	<?php echo form_open_multipart(base_url().'admin/upload');?>

		Select File : <input type="file" accept=".pdf" name="magfile" required />(<b>PDF ONLY</b>)

		<br />
		Select Month & Year of Edition :
		<select name="month" required
    oninvalid="this.setCustomValidity('Select Month First')"
    oninput="this.setCustomValidity('')">
			<option value="" selected>Select</option>
			<option>January</option>
			<option>February</option>
			<option>March</option>
			<option>April</option>
			<option>May</option>
			<option>June</option>
			<option>July</option>
			<option>August</option>
			<option>September</option>
			<option>October</option>
			<option>November</option>
			<option>December</option>
			<!-- 
			<option<?=(date("M")=="January"?' selected':'')?>>January</option>
			<option<?=(date("M")=="February"?' selected':'')?>>February</option>
			<option<?=(date("M")=="March"?' selected':'')?>>March</option>
			<option<?=(date("M")=="April"?' selected':'')?>>April</option>
			<option<?=(date("M")=="May"?' selected':'')?>>May</option>
			<option<?=(date("M")=="June"?' selected':'')?>>June</option>
			<option<?=(date("M")=="July"?' selected':'')?>>July</option>
			<option<?=(date("M")=="August"?' selected':'')?>>August</option>
			<option<?=(date("M")=="September"?' selected':'')?>>September</option>
			<option<?=(date("M")=="October"?' selected':'')?>>October</option>
			<option<?=(date("M")=="November"?' selected':'')?>>November</option>
			<option<?=(date("M")=="December"?' selected':'')?>>December</option> -->
		</select>
		<select name="year" required>
			<?php
				$cy = date("Y");
				for ($i=1989; $i <= 2050; $i++) { 
					?>
			<option<?=(date("Y")==$i?' selected':'')?>><?=$i?></option><?php
				}
			?>
		</select>

		<br /><br />

		<input type="submit" value="upload" />

	</form>

	<hr>

	<div style="position: fixed;bottom: 0;left: 0;right: 0;height: 1.2rem;font-size: 1rem;text-align: center;background: #e4e4e4;color: #111;">Designed & Developed By <a href="http://www.mandalsoftwares.cf" target="_blank">Mandal</a></div>
</body>
</html>