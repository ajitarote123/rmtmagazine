<?php 
//print_r($_SERVER);
try{
  $file = FCPATH . ".magfiles/" . "RMTKPOAH_Magazine";

  if (!isset($_REQUEST['edition'])) {
    header("HTTP/1.0 404 Not Found");
    ?>
    <!DOCTYPE html>
    <html>
    <head>
      <title>Error</title>
    </head>
    <body style="text-align: center;">
      <h1>Sorry, some error occurred.</h1><br>
      <a href="<?=base_url()?>">Home</a>
    </body>
    </html>
    <?php
    return;
  }

  if (strtoupper($_REQUEST['edition']) == "LATEST") {
    $file .= "_" . date("M") . date("y") . ".pdf";
  } else {
    $file .= "_" . ucfirst($_GET['edition']) . ".pdf";
  }
  //echo $file;

  if(file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Length: ' . filesize($file));

    readfile($file);
    return;
  } else {
    header("HTTP/1.0 404 Not Found");
    ?>
    <!DOCTYPE html>
    <html>
    <head>
      <title>Error</title>
    </head>
    <body style="text-align: center;">
      <h1>Sorry, the file you requested is not available.</h1><br>
      <a href="<?=base_url()?>">Home</a>
    </body>
    </html>
    <?php
  }
} catch(Exception $e) {
  header("HTTP/1.0 404 Not Found");
?>
    <!DOCTYPE html>
    <html>
    <head>
      <title>Error</title>
    </head>
    <body style="text-align: center;">
      <h1>Sorry, some error occurred.</h1><br>
      <a href="<?=base_url()?>">Home</a>
    </body>
    </html>
    <?php
}
  
?>